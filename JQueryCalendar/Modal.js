function Modal() {
    var self = this;
    var $modal = null;
    var editView = false;
    var oldEvent = null;
    var currentDate = null;
    var onSaveCallback = null;
    var onDeleteCallback = null;

    self.init = function ($container) {
        $modal = $container;
        $container.find(".cancel").click(function (e) {
            e.preventDefault();
            close(); 
        });
        $container.find(".btn-primary").click(function (e) {
            e.preventDefault();
            saveEvent();
        });
        $container.find("#deleteBtn").click(function (e) {
            e.preventDefault();
            deleteEvent();
        });
    };

    self.show = function (event, deleteBtn) {
        
        editView = false;
        $modal.show();
        $modal.find("h3").text("New Event");
        $(document).find("#backdrop").css("opacity", ".3");

        if (deleteBtn) {
            oldEvent = event;
            editView = true;
            setEventData(event);
            event = event.date;
        }
        currentDate = event;
    };

    self.onSave = function (callback) {
        onSaveCallback = callback;
    };

    self.onDelete= function (callback) {
        onDeleteCallback = callback;
    };

    function close() {
        $(document).find("#backdrop").css("opacity", "1");
        $modal.find(".time-picker").val("");
        $modal.find("#name").val("");
        $modal.hide();
        $modal.find("#deleteBtn").hide();
    }

    function saveEvent() {
        var time = $modal.find(".time-picker").val();
        var hours = time.split(":")[0];
        var mins = time.split(":")[1];
        var date = new Date(currentDate);
        date.setHours(hours, mins, 0, 0);

        var name = $modal.find("#name").val();
        if (onSaveCallback) {
            onSaveCallback(name, date, oldEvent, editView);
        }
        close();
    }

    function deleteEvent() {
        var time = $modal.find(".time-picker").val();
        var hours = time.split(":")[0];
        var mins = time.split(":")[1];
        var date = new Date(currentDate);
        date.setHours(hours, mins, 0, 0);

        var name = $modal.find("#name").val();
        if (onDeleteCallback) {
            onDeleteCallback(name, date, editView);
        }
        close();
    }

    function setEventData(event) {
        $modal.find("#deleteBtn").show();
        $modal.find("h3").text("Edit Event");
        $modal.find(".time-picker").val(configureTime(event.date));
        $modal.find("#name").val(event.eventName);
    }

    function configureTime(date) {
        var hours = date.getHours();
        var mins = date.getMinutes();
        if (hours < 10)
            hours = "0" + hours;
        if (mins < 10)
            mins = "0" + mins;
        return (hours + ":" + mins + ":00")
    }
}