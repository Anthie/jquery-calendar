﻿function Calendar() {
    var self = this;
    var $calendar = null;
    var _currentMonth = null;
    var onDayClickedCallback = null;
    var onEventClickedCallback = null;
    var deleteBtn = null;
    var events = [];

    self.init = function ($container) {
        $calendar = $container
        setCurrentMonth(new Date(), 0);
        $container.find("#prevMonthButton").click(setPreviousMonth);
        $container.find("#nextMonthButton").click(setNextMonth);
        $container.on("click", ".day", function (e) {
            deleteBtn = false;
            onDayClickedCallback($(e.currentTarget).data().date, deleteBtn);
        });
        $container.on("click", ".label", function (e) {
            deleteBtn = true;
            e.stopPropagation();
            onEventClickedCallback($(e.currentTarget).data().event, deleteBtn);
        });
    };

    self.onDayClicked = function (callback) {
        onDayClickedCallback = callback;
    };

    self.onEventClicked = function (callback) {
        onEventClickedCallback = callback;
    };

    self.updateEvents = function (eventsCollection) {
        events = eventsCollection;
        drawDays();
    };

    function renderEvents() {
        for (var i = 0; i < events.length; i++) {
            var event = events[i];
            var $element = getElementForDate(event.date);
            var ampm = (event.date.getHours() >= 12) ? "PM" : "AM";
            $("<div class='label label-default'></div>").text(hours12(event.date) + ":" + minutes(event.date.getMinutes()) + " " + ampm + " " + event.eventName).data("event", event).appendTo($element);
        }
    }

    function hours12(date) { 
        return (date.getHours() + 24) % 12 || 12; 
    }

    function minutes(min) {
        if (min === 0)
            return "00";
        else if (min < 10)
            return "0" + min;
        return min;
    }

    function getElementForDate(date) {
        return $calendar
            .find(".day")
            .filter(function () { return doesElementDateEqualDate(this, date); });
    }

    function doesElementDateEqualDate(element, date) {
        var elementDate = $(element).data().date;
        return areDatesEqual(elementDate, date);
    }

    function areDatesEqual(a, b) {
        return a.getFullYear() === b.getFullYear()
            && a.getMonth() === b.getMonth()
            && a.getDate() === b.getDate();
    }

    function setPreviousMonth() {
        setCurrentMonth(_currentMonth, -1);
    }

    function setNextMonth() {
        setCurrentMonth(_currentMonth, 1);
    }

    function setCurrentMonth(date, index) {
        _currentMonth = date;
        _currentMonth.setMonth(_currentMonth.getMonth() + index, 1);
        setHeading();
        drawDays();
    }

    function setHeading() {
        $("#calendar-header").empty();
        var month = monthToString(_currentMonth.getMonth());
        var year = _currentMonth.getFullYear();
        $h1tag = $("<h1></h1>").appendTo("#calendar-header");
        $h1tag.text(month + " " + year);
    }

    function monthToString(index) {
        var m_names = new Array("January", "February", "March",
        "April", "May", "June", "July", "August", "September",
        "October", "November", "December");

        return m_names[index];
    }

    function drawDays() {
        var currentYear = _currentMonth.getFullYear();
        var currentMonth = _currentMonth.getMonth();
        var numDays = daysInMonth(currentMonth, currentYear);
        var currentWeek = null;
        var weeks = [];

        for (var i = 1; i <= numDays; i++) {
            if (!currentWeek) {
                currentWeek = new Week();
            }

            var currentDate = new Date(currentYear, currentMonth, i);
            if (currentWeek.isFull()) {
                weeks.push(currentWeek);
                currentWeek = new Week();
            }
            currentWeek.addDate(currentDate);
        }
        if (!currentWeek.isEmpty()) {
            if (!currentWeek.isFull()) {
                var currentDate = new Date(currentYear, currentMonth, numDays);
                currentWeek.fillAdditionalDates(currentDate);
            }
            weeks.push(currentWeek);
        }

        createDays(weeks);
        renderEvents();
    }

    function createDays(weeks) {
        $(".body").empty();
        for (var i = 0; i < weeks.length; i++) {
            var $week = $("<div class='row week'></div>").appendTo($(".body"));
            var week = weeks[i];
            for (var j = 0; j < week.getDates().length; j++) {
                var date = week.getDates()[j];
                var $day = $("<div class='col-xs-1 day'></div>").data("date", date).appendTo($week);
                $("<h6></h6>").text(date.getDate()).appendTo($day);
                if (date.getMonth() != _currentMonth.getMonth()) {
                    $day.addClass("other-month-days");
                }
            }
        }
    }

    function daysInMonth(month, fullYear) {
        return new Date(fullYear, month + 1, 0).getDate();
    }



    //
    // Week class
    //
    function Week() {
        var self = this;
        var dates = [];

        self.addDate = function (date) {
            addMissingDatesIfNeeded(date);
            dates.push(date);
        };

        self.isFull = function () {
            return dates.length === 7;
        };

        self.isEmpty = function () {
            return dates.length === 0;
        };

        self.getDates = function () {
            return dates;
        }

        self.fillAdditionalDates = function(date) {
            var numDatesNeeded = 6 - date.getDay();

            for (var i = 1; i <= numDatesNeeded; i++) {
                var dateToAdd = new Date(date);
                dateToAdd.setDate(date.getDate() + i);
                dates.push(dateToAdd);
            }
        }

        function addMissingDatesIfNeeded(date) {
            var needPrevousDates = date.getDay() != dates.length;
            
            if (needPrevousDates) {
                fillPreviousDates(date);
            }
        }

        function fillPreviousDates(date) {
            var numDatesNeeded = date.getDay() - dates.length;

            for (var i = 0; i < numDatesNeeded; i++) {
                var dateToAdd = new Date(date);
                var numDaysInPast = numDatesNeeded - i;
                dateToAdd.setDate(date.getDate() - numDaysInPast);
                dates.push(dateToAdd);
            }
        }
    }
}



